# This file is part of the purchase_payment module for Tryton.
# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond import backend
from trytond.model import ModelSQL, ModelView, fields
from trytond.wizard import Button, StateReport 
from trytond.pyson import Eval


__all__ = ['PurchaseDevice', 'PurchaseDeviceStatementJournal']


class PurchaseDevice(ModelSQL, ModelView):
    'Purchase Device Configuration'
    __name__ = 'purchase.device'
    name = fields.Char('Device Name', required=True, select=True)
    shop = fields.Many2One('purchase.shop', 'Shop', required=True)
    company = fields.Function(fields.Many2One('company.company', 'Company',),
        'get_company', searcher='search_company')
    journals = fields.Many2Many('purchase.device.account.statement.journal',
        'device', 'journal', 'Journals', depends=['company'],
        domain=[
            ('company', '=', Eval('company')),
            ]
        )
    journal = fields.Many2One('account.statement.journal', "Default Journal",
        ondelete='RESTRICT', depends=['journals'],
        domain=[('id', 'in', Eval('journals', []))],
        )
    users = fields.One2Many('res.user', 'purchase_device', 'Users')
    change_unit_price = fields.Boolean('Change of Price')
    allow_credit = fields.Boolean('Allow Credit')

    @classmethod
    def __register__(cls, module_name):

        old_table = 'purchase_pos_device'
        if backend.TableHandler.table_exist(old_table):
            backend.TableHandler.table_rename(old_table, cls._table)

        super(PurchaseDevice, cls).__register__(module_name)

    def default_allow_credit():
        return False

    @fields.depends('shop')
    def on_change_shop(self):
        self.company = self.shop.company.id if self.shop else None

    def get_company(self, name):
        return self.shop.company.id

    @classmethod
    def search_company(cls, name, clause):
        return [('shop.%s' % name,) + tuple(clause[1:])]


class PurchaseDeviceStatementJournal(ModelSQL):
    'Purchase Device - Statement Journal'
    __name__ = 'purchase.device.account.statement.journal'
    _table = 'purchase_device_account_statement_journal'
    device = fields.Many2One('purchase.device', 'Purchase Device',
            ondelete='CASCADE', select=True, required=True)
    journal = fields.Many2One('account.statement.journal', 'Statement Journal',
            ondelete='RESTRICT', required=True)

    @classmethod
    def __register__(cls, module_name):
        table = backend.TableHandler(cls, module_name)

        old_table = 'purchase_pos_device_account_statement_journal'
        if backend.TableHandler.table_exist(old_table):
            backend.TableHandler.table_rename(old_table, cls._table)

        old_column = 'pos_device'
        new_column = 'device'
        if table.column_exist(old_column):
            table.drop_fk(old_column)
            table.column_rename(old_column, new_column)

        super(PurchaseDeviceStatementJournal, cls).__register__(module_name)
