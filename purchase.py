# This file is part of the purchase_payment module for Tryton.
# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from decimal import Decimal
from sql.aggregate import Sum
from sql.conditionals import Coalesce

from trytond.model import ModelView, fields
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Bool, Eval, Not
from trytond.transaction import Transaction
from trytond.wizard import Wizard, StateView, StateTransition, Button
from trytond.i18n import gettext
from trytond.exceptions import UserError


__all__ = ['Purchase', 'PurchasePaymentForm', 'WizardPurchasePayment',
    'WizardPurchaseReconcile']


class Purchase(metaclass=PoolMeta):
    __name__ = 'purchase.purchase'
    payments = fields.One2Many('account.statement.line', 'purchase', 'Payments')
    paid_amount = fields.Function(fields.Numeric('Paid Amount', readonly=True),
        'get_paid_amount')
    residual_amount = fields.Function(fields.Numeric('Residual Amount'),
        'get_residual_amount', searcher='search_residual_amount')
    purchase_device = fields.Many2One('purchase.device', 'Purchase Device',
            domain=[('shop', '=', Eval('shop'))],
            depends=['shop'], states={
                'readonly': Eval('state') != 'draft',
            }
                                      )

    @classmethod
    def __setup__(cls):
        super(Purchase, cls).__setup__()
        cls._buttons.update({
                'wizard_purchase_payment': {
                    'invisible': Eval('state') == 'done',
                    'readonly': Not(Bool(Eval('lines'))),
                    },
                })

    @staticmethod
    def default_purchase_device():
        User = Pool().get('res.user')
        user = User(Transaction().user)
        return user.purchase_device and user.purchase_device.id or None

    def set_basic_values_to_invoice(self, invoice):
        pool = Pool()
        Date = pool.get('ir.date')
        today = Date.today()
        if not getattr(invoice, 'invoice_date', False):
            invoice.invoice_date = today
        if not getattr(invoice, 'accounting_date', False):
            invoice.accounting_date = today
        invoice.description = self.reference

    @classmethod
    def set_invoices_to_be_posted(cls, purchases):
        pool = Pool()
        Invoice = pool.get('account.invoice')
        invoices = []
        to_post = set()
        for purchase in purchases:
            grouping = getattr(purchase.party, 'purchase_invoice_grouping_method',
                False)
            if getattr(purchase, 'invoices', None) and not grouping:
                for invoice in purchase.invoices:
                    if not invoice.state == 'draft':
                        continue
                    purchase.set_basic_values_to_invoice(invoice)
                    invoices.extend(([invoice], invoice._save_values))
                    to_post.add(invoice)

        if to_post:
            Invoice.write(*invoices)
            return list(to_post)

    @classmethod
    def workflow_to_end(cls, purchases):
        pool = Pool()
        StatementLine = pool.get('account.statement.line')
        Invoice = pool.get('account.invoice')

        for purchase in purchases:
            if purchase.state == 'draft':
                cls.quote([purchase])
            if purchase.state == 'quotation':
                cls.confirm([purchase])
            if purchase.state == 'confirmed':
                cls.process([purchase])

            if not purchase.invoices and purchase.invoice_method == 'order':
                raise UserError(gettext(
                    'purchase_payment.not_customer_invoice',
                        reference=purchase.reference))

        to_post = cls.set_invoices_to_be_posted(purchases)
        if to_post:
            Invoice.post(to_post)

        to_save = []
        to_do = []
        for purchase in purchases:
            posted_invoice = None
            for invoice in purchase.invoices:
                if invoice.state == 'posted':
                    posted_invoice = invoice
                    break
            if posted_invoice:
                for payment in purchase.payments:
                    # Because of account_invoice_party_without_vat module
                    # could be installed, invoice party may be different of
                    # payment party if payment party has not any vat
                    # and both parties must be the same
                    if payment.party != invoice.party:
                        payment.party = invoice.party
                    payment.invoice = posted_invoice
                    to_save.append(payment)

            if purchase.is_done():
                to_do.append(purchase)

        StatementLine.save(to_save)

        if to_do:
            cls.do(to_do)

    @classmethod
    def get_paid_amount(cls, purchases, names):
        result = {n: {s.id: Decimal(0) for s in purchases} for n in names}
        for name in names:
            for purchase in purchases:
                for payment in purchase.payments:
                    result[name][purchase.id] += -payment.amount
        return result

    @classmethod
    def get_residual_amount(cls, purchases, name):
        return {s.id: s.total_amount - s.paid_amount if s.state != 'cancelled'
            else Decimal(0) for s in purchases}

    @classmethod
    def search_residual_amount(cls, name, clause):
        pool = Pool()
        Purchase = pool.get('purchase.purchase')
        StatementLine = pool.get('account.statement.line')

        purchase = Purchase.__table__()
        payline = StatementLine.__table__()
        Operator = fields.SQL_OPERATORS[clause[1]]
        value = clause[2]

        query = purchase.join(
            payline,
            type_='LEFT',
            condition=(purchase.id == payline.purchase)
            ).select(
                purchase.id,
                where=((purchase.total_amount_cache != None) &
                    (purchase.state.in_([
                        'draft',
                        'quotation',
                        'confirmed',
                        'processing',
                        'done']))),
                group_by=(purchase.id),
                having=(
                    (Sum(Coalesce(payline.amount, 0)) < purchase.total_amount_cache)
                & Operator(purchase.total_amount_cache -
                    Sum(Coalesce(payline.amount, 0)), value)
                ))

        return [('id', 'in', query)]

    @classmethod
    @ModelView.button_action('purchase_payment.wizard_purchase_payment')
    def wizard_purchase_payment(cls, purchases):
        pass

    @classmethod
    def copy(cls, purchases, default=None):
        if default is None:
            default = {}
        default['payments'] = None
        return super(Purchase, cls).copy(purchases, default)


class PurchasePaymentForm(ModelView):
    'Purchase Payment Form'
    __name__ = 'purchase.payment.form'
    journal = fields.Many2One('account.statement.journal', 'Statement Journal',
        domain=[
            ('id', 'in', Eval('journals', [])),
            ],
        depends=['journals'], required=True)
    journals = fields.One2Many('account.statement.journal', None,
        'Allowed Statement Journals')
    payment_amount = fields.Numeric('Payment amount', required=True,
        digits=(16, Eval('currency_digits', 2)),
        depends=['currency_digits'])
    currency_digits = fields.Integer('Currency Digits')
    party = fields.Many2One('party.party', 'Party', readonly=True)       

    
class WizardPurchasePayment(Wizard):
    'Wizard Purchase Payment'
    __name__ = 'purchase.payment'
    start = StateView('purchase.payment.form',
        'purchase_payment.purchase_payment_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Pay', 'pay_', 'tryton-ok', default=True),
        ])
    pay_ = StateTransition()

    
    def default_start(self, fields):
        pool = Pool()
        Purchase = pool.get('purchase.purchase')
        User = pool.get('res.user')
        purchase = Purchase(Transaction().context['active_id'])
        user = User(Transaction().user)
        purchase_device = purchase.purchase_device or user.purchase_device or False
        if user.id != 0 and not purchase_device:
            raise UserError(gettext('purchase_payment.not_purchase_device'))
        return {
            'journal': purchase_device.journal.id
                if purchase_device.journal else None,
            'journals': [j.id for j in purchase_device.journals],
            'payment_amount': purchase.total_amount - purchase.paid_amount
                if purchase.paid_amount else purchase.total_amount,
            'currency_digits': purchase.currency_digits,
            'party': purchase.party.id,
            }

    def get_statement_line(self, purchase):
        pool = Pool()
        Date = pool.get('ir.date')
        Purchase = pool.get('purchase.purchase')
        Statement = pool.get('account.statement')
        StatementLine = pool.get('account.statement.line')

        form = self.start
        statements = Statement.search([
                ('journal', '=', form.journal),
                ('state', '=', 'draft'),
                ], order=[('date', 'DESC')])
        if not statements:
            raise UserError(gettext('purchase_payment.not_draft_statement',
                journal=form.journal.name))

        if not purchase.number:
            Purchase.set_number([purchase])

        with Transaction().set_context(date=Date.today()):
            account = purchase.party.account_payable_used

        if not account:
            raise UserError(gettext(
                'purchase_payment.party_without_account_payable',
                    party=purchase.party.name))

        if form.payment_amount:
            return StatementLine(
                statement=statements[0],
                date=Date.today(),
                amount=-form.payment_amount,
                party=purchase.party,
                invoice=purchase.invoices[0].id if purchase.invoices else None, 
                account=account,
                description=purchase.number,
                purchase=purchase,
                )

    def transition_pay_(self):
        Purchase = Pool().get('purchase.purchase')

        active_id = Transaction().context.get('active_id', False)
        purchase = Purchase(active_id)

        if purchase.state != 'draft':
            line = self.get_statement_line(purchase)
            if line:
                line.save()
            return 'end'
        else:
            purchase.description = purchase.reference
            purchase.save()
            Purchase.workflow_to_end([purchase])

        line = self.get_statement_line(purchase)
        if line:
            line.save()

        if purchase.total_amount != purchase.paid_amount:
            return 'start'

        return 'end'
