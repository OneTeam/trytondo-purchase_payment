# This file is part of the purchase_payment module for Tryton.
# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond import backend
from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.pyson import Eval


__all__ = ['User']


class User(metaclass=PoolMeta):
    __name__ = "res.user"
    purchase_device = fields.Many2One('purchase.device', 'Purchase Device',
            domain=[('shop', '=', Eval('shop'))],
            depends=['shop']
    )

    @classmethod
    def __setup__(cls):
        super(User, cls).__setup__()
        if 'purchase_device' not in cls._preferences_fields:
            cls._preferences_fields.extend([
                    'purchase_device',
                    ])

    @classmethod
    def __register__(cls, module_name):
        table = backend.TableHandler(cls, module_name)

        # Migrate from purchase_pos 3.0
        old_column = 'pos_device'
        new_column = 'purchase_device'
        if table.column_exist(old_column):
            table.drop_fk(old_column)
            table.column_rename(old_column, new_column)

        super(User, cls).__register__(module_name)

    def on_change_company(self):
        super().on_change_company()
        self.purchase_device = None
