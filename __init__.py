# This file is part of the purchase_payment module for Tryton.
# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from . import device
from . import purchase
from . import statement
from . import user


def register():
    Pool.register(
        statement.Journal,
        statement.Statement,
        statement.Line,
        device.PurchaseDevice,
        user.User,
        device.PurchaseDeviceStatementJournal,
        purchase.Purchase,
        purchase.PurchasePaymentForm,
        statement.OpenStatementStart,
        statement.OpenStatementDone,
        statement.CloseStatementStart,
        statement.CloseStatementDone,
        module='purchase_payment', type_='model')
    Pool.register(
        purchase.WizardPurchasePayment,
        statement.OpenStatement,
        statement.CloseStatement,
        module='purchase_payment', type_='wizard')
